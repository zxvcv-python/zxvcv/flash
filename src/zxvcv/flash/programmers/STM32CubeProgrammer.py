import logging
import subprocess
from pathlib import Path

from zxvcv.util.Path import context_cwd

from .Programmer import Programmer

LOGGER_NAME = __package__
log = logging.getLogger(LOGGER_NAME)


class Connection():
    def __init__(self, port:str="SWD", freq:int=4000, ap:int=0) -> None:
        # TODO[PP]: validation of connection parameters
        self.port = port
        self.freq = freq
        self.ap = ap

    def get_param(self):
        return f"-c port={self.port} freq={self.freq} ap={self.ap}"

class STM32CubeProgrammer(Programmer):
    _EXE = "STM32_Programmer_CLI.exe"
    def __init__(self, path:Path, port:str="SWD", freq:int=4000, ap:int=0, verbose:int=1, log_file:Path=Path.cwd()/"flash/falsh_all.log") -> None:
        self.bin_dir = path/"bin"

        # connection settings
        self.connection = Connection(port, freq, ap)

        # log settings
        self.verbose = verbose
        self.log_file = log_file

    def program_file(self, filepath:str, address:int=0x08000000, wkdir:Path=Path.cwd()):
        with context_cwd(self.bin_dir):
            cmd = [
                self._EXE,
                self.connection.get_param(),
                f"--write {(wkdir/filepath).resolve()} {hex(address)}",
                f"--log {wkdir/self.log_file}",
                f"--verbose {self.verbose}"
            ]
            log.debug(" ".join(cmd))

            process = subprocess.Popen(" ".join(cmd),
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                shell = True
            )
            for line in process.stdout:
                print(line.decode("utf-8").strip())
