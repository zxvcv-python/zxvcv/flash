import sys
import logging
import argparse
import platform
from pathlib import Path
from datetime import datetime
from zxvcv.flash.programmers.Programmer import Programmer

from zxvcv.util import ArgsManager, LogManager

from .programmers.Programmer import Programmer
from .programmers.STM32CubeProgrammer import STM32CubeProgrammer

LOGGER_NAME = __package__
log = logging.getLogger(LOGGER_NAME)


class LocalArgsManager(ArgsManager):
    class ParseSTM32ProgrammerArgument(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            values = eval(values)
            setattr(namespace, self.dest, values)

    class ParseSTM32ProgrammerPathArgument(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            values = Path(values)
            setattr(namespace, self.dest, values)

        @staticmethod
        def get_default() -> Path:
            if platform.system() == "Windows":
                return Path(r"C:\Program Files\STMicroelectronics\STM32Cube\STM32CubeProgrammer")
            elif platform.system() == "Linux":
                raise ValueError("Linux system not supprted yet.")

    def _add_stm32_subcommand(self, parser):
        command_parser = parser.add_parser("stm32",
            help="Flashing manager for stm32."
        )

        command_parser.add_argument("--workspace", type=Path,
            default=Path.cwd(),
            help="Root directory of the project to flash."
        )

        command_parser.add_argument("--programmer", type=Programmer,
            default=STM32CubeProgrammer,
            choices=["STM32CubeProgrammer"],
            action=LocalArgsManager.ParseSTM32ProgrammerArgument,
            help="Name of the programmer to beused during programming the device."
        )

        command_parser.add_argument("--programmer-path", type=Path,
            default=LocalArgsManager.ParseSTM32ProgrammerPathArgument.get_default(),
            dest="programmer_path",
            action=LocalArgsManager.ParseSTM32ProgrammerPathArgument,
            help="Path to the programmer, that will be used to program the device."
        )

        command_parser.add_argument("--binary-path", type=Path,
            default=Path(r"bld\bin\tests_embd1.hex"),
            dest="binary_path",
            help="Path to the binary file that would be flashed into memory. Relative to working dir."
        )

        super()._add_logging_arguments(command_parser)

    def get_parser(self) -> argparse.ArgumentParser:
        self.subparsers = self.parser.add_subparsers(required=True, dest="target")
        self._add_stm32_subcommand(self.subparsers)
        return self.parser

def main(argv=sys.argv[1:]):
    if platform.system() == "Windows":
        pass
    elif platform.system() == "Linux":
        raise RuntimeError("Linux is not supported yet.")
    else:
        raise ValueError("Unsupported system.")

    args = LocalArgsManager("zxvcv.flash").get_parser().parse_args(argv)
    LogManager.setup_logger(LOGGER_NAME, level=int(args.log))
    log.debug(f"Script arguments: {args}")

    programmer = args.programmer(
        path=args.programmer_path,
        log_file=args.workspace/f"bld/flash/falsh_all{datetime.now().strftime('_%d_%m_%Y__%H_%M_%S')}.log"
    )
    programmer.program_file(
        filepath=args.binary_path,
        wkdir=args.workspace
    )

if __name__.rpartition(".")[-1] == "__main__":
    sys.exit(main(sys.argv[1:]))
